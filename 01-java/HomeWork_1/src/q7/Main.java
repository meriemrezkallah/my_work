package q7;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 ArrayList<Employees> ar = new ArrayList<Employees>();
		// Employees e = new Employees(1, "hh", "Lahlou");
		 //Employees e2 = new Employees(12, "plk", "Amir");
		 ar.add(new Employees(27, "HR", "Lahlou"));
	     ar.add(new Employees(111, "Finance", "Amir"));
	     //sort by name
	     Collections.sort(ar,new Sortbyname());
	     System.out.println("\nSorted by name");
	        for (int i = 0; i < ar.size(); i++)
	            System.out.println(ar.get(i));
	      //sort by age
	      Collections.sort(ar,new Sortbyage());
		  System.out.println("\nSorted by age");
		        for (int i = 0; i < ar.size(); i++)
		            System.out.println(ar.get(i));
		   //sort by Dep
			      Collections.sort(ar,new Sortbydep());
				  System.out.println("\nSorted by Departement");
				        for (int i = 0; i < ar.size(); i++)
				            System.out.println(ar.get(i));
	}

}

