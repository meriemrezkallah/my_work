package q7;

import java.util.Comparator;

class Employees {
	 
	int age;
	String dep,name;
	// constractor
	public Employees(int age, String dep, String name) {
		super();
		this.age = age;
		this.dep = dep;
		this.name = name;
	}
	//to print info in main methode 
	@Override
	public String toString() {
		return "\nEmployees [age=" + age + ", dep=" + dep + ", name=" + name + "]";
	}
}

////////// sort by age
	 class Sortbyage implements Comparator<Employees> {
	    // Used for sorting in ascending order of
	    // roll number
	    public int compare(Employees a, Employees b)
	    {
	        return a.age - b.age;
	    }
	}
//////////////////////Sort by name
	 class Sortbyname implements Comparator<Employees> {
	    // Used for sorting in ascending order of
	    // name
	    public int compare(Employees a, Employees b)
	    {
	        return a.name.compareTo(b.name);
	    }
	}
	
//////////////////////sort by dep
	class Sortbydep implements Comparator<Employees> {
	    // Used for sorting in ascending order of
	    // name
	    public int compare(Employees a, Employees b)
	    {
	        return a.dep.compareTo(b.dep);
	    }
	}