package q20;

import java.io.File;
import java.util.Scanner;

public class ReadFile {
 private Scanner x;
 public void openFile() {
	 try {
		x = new Scanner(new File("data.txt"));
	} catch (Exception e) {
		System.out.println("Could not find the file");
		// TODO: handle exception
	}
 }
 public void readFile() {
	 String name1,name2,age,state;
	 while(x.hasNext()) {
		
		 String a=x.next();
		 String[] xx = a.substring(0, a.length()).split(":");
		 for (int i=0;i<xx.length;i++) {
		// System.out.println(a.length());
			 if (i==0) {
				 System.out.print("Name: ");
				 name1=xx[i];
				 System.out.print(name1+" ");
			 }
			 if (i==1) {
				 name2=xx[i];
				 System.out.print(name2 +"\n");
			 }
			 if (i==2) {
				 age=xx[i];
				 System.out.println("Age: "+age+" Years");
			 }
			 if (i==3) {
				 state=xx[i];
				 System.out.println("State: "+state+" State");
			 }
	     //System.out.printf("%s \n",xx[i]);
		 }
		 
		System.out.println("------------------------------");
		 
	 }
 }
 public void closeFile() {
	 x.close();
 }
}
