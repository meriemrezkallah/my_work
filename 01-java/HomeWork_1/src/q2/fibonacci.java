package q2;

public class fibonacci {

	static void fibo(int n) {
		int  firstTerm = 0, secondTerm = 1;
		System.out.println("Fibonacci Series for the " + n + " terms:");

	    for (int i = 1; i <= n; ++i) {
	      System.out.print(firstTerm + ", ");

	      // compute the next term
	      int nextTerm = firstTerm + secondTerm;
	      firstTerm = secondTerm;
	      secondTerm = nextTerm;
	    }
	}
		// TODO Auto-generated
	  public static void main(String[] args) {
		  //fibonacci for the 25 first number
      fibo(25);
	    
	    }
	  
}