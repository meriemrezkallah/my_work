-- ----------------------------
-- 2.1
-- ----------------------------
select * from employee;
select * from employee where last_name='King';
select * from employee where first_name='Andrew' and reports_to isnull;
-- ----------------------------
-- 2.2
-- ----------------------------
select * from album order by title desc;
select first_name from customer order by city asc;

-- ----------------------------
-- 2.3
-- ----------------------------
select * from genre;
insert into genre values (26,'Malouf');
insert into genre values (27,'Andalous');
select * from genre;


select * from employee;
insert into employee values (9,'Meriem','IT Staff','1993-10-30');
insert into employee values (10,'Elyas','IT Staff','2020-10-02');

select * from customer;
insert into customer values (60,'Lahlou','Kadri','1726 Sotogrande BLVD','Hurst','Texas','USA','76053','021244230','','fksdfk@gamil.com','');
insert into customer values (61,'Amir','Kadri','1726 Sotogrande BLVD','Hurst','Texas','USA','76053','0775352388','','amirkad@gamil.com','');
select * from customer;

-- ----------------------------
-- 2.4
-- ----------------------------
select * from customer;
update customer set first_name='Robert',last_name='Walter' where first_name='Aaron' and last_name='Mitchell';
select * from artist;
update artist set name='CCR' where name='Creedence Clearwater Revival';


-- ----------------------------
-- 2.5
-- ----------------------------
select * from invoice where billing_address like 'T%'

-- ----------------------------
-- 2.6
-- ----------------------------

select * from invoice where total between 15 and 50;

select * from employee;
select * from employee where hire_date between '2003-06-01' and '2004-03-01';

-- ----------------------------
-- 2.7
-- ----------------------------
select * from invoice;
select * from invoice_line;
delete from invoice_line where invoice_id =50;
delete from invoice where customer_id =32 and invoice_id=50;
delete from customer where first_name='Robert' and last_name='Walter';




-- ----------------------------
-- 3.1
-- ----------------------------
select current_date;
select current_time;
 
select * from media_type;

select count(*)  from media_type;

-- ----------------------------
-- 3.2
-- ----------------------------

select * from invoice;
select avg(total) from invoice;

select * from track;
select max(milliseconds*unit_price) as Expensive_truck from track;

-- ----------------------------
-- 3.3
-- ----------------------------
select * from invoice_line;
select avg(unit_price*quantity) from invoice_line;

-- ----------------------------
-- 3.4
-- Create a statement that returns all employees who are born after 1968.
-- ----------------------------
select * from employee;
select * from employee where birth_date >= '1968-01-01';

-- ----------------------------
-- 3.6
--Create an inner join that joins customers and orders and specifies the name of the customer and the invoiceId.
-- ----------------------------

SELECT c.first_name,c.last_name,i.invoice_id 
FROM  customer c 
INNER JOIN invoice i
ON c.customer_id = i.customer_id;

-- ----------------------------
-- 3.7
--– Create an outer join that joins the customer and invoice table, specifying the CustomerId, firstname, lastname, invoiceId, and total.
-- ----------------------------
select c.customer_id,c.first_name,c.last_name,i.invoice_id,i.total 
from customer c 
FULL OUTER join invoice i 
on c.customer_id = i.customer_id;

-- ----------------------------
-- 3.8 Task – Create a right join that joins album and artist specifying artist name and title.
--– 
-- ----------------------------
SELECT a.title,a2.name 
FROM album a 
RIGHT JOIN artist a2 
ON a.artist_id = a2.artist_id ;


-- ----------------------------
-- 3.9
--– Create a cross join that joins album and artist and sorts by artist name in ascending order.
-- ----------------------------


SELECT *
FROM album a 
     CROSS JOIN artist a2
order by a2.name asc;








----------------------------------------------------
------------Perform a self-join on the employee table, joining on the reportsto column.
------------------------------------------------------------


SELECT *
FROM employee e,employee e2 
WHERE e.reports_to = e2.reports_to ;

----------------------------------------------------------------------------------
SELECT  *
FROM INFORMATION_SCHEMA.columns
WHERE TABLE_NAME LIKE '%' AND COLUMN_NAME LIKE '%';

   
 